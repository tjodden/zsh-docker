FROM alpine:latest
RUN apk --no-cache add zsh
CMD ["/bin/zsh"]
